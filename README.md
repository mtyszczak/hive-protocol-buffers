# hive-protocol-buffers

Hive protocol as Google Protobuf

## Building

### Building on Ubuntu 20.04

```bash
# install dependencies
sudo apt update

sudo apt install -y \
    git \
    cmake \
    make \
    g++ \
    libssl-dev \
    libboost-all-dev \
    libzstd-dev \
    libreadline-dev \
    libsnappy-dev \
    libprotobuf-dev \
    protobuf-compiler

git clone https://gitlab.com/mtyszczak/hive-protocol-buffers.git
cd hive-protocol-buffers
git checkout main

mkdir build
cd build

git submodule update --init --recursive --progress

cmake -DCMAKE_BUILD_TYPE=Release ..
sudo make -j$(nproc)

# Optional
sudo make install # install defaults to /usr/local
```

## Testing

### Testing on Ubuntu 20.04

You should first build the program

```bash
sudo apt update

sudo apt install -y \
    libgtest-dev

cmake -DCMAKE_BUILD_TYPE=Release -DHPB_BUILD_TESTS=ON ..
sudo make -j$(nproc)

ctest
```

## License

Distributed under the GNU GENERAL PUBLIC LICENSE Version 3
See [LICENSE.md](LICENSE.md)
