#include <gtest/gtest.h>

#include <string>
#include <vector>

#include <hive/protocol/transaction.hpp>

#include "transaction.pb.h"

#include <fc/io/raw.hpp>

namespace {
using namespace hive::protocol;

buffers::operation get_protobuf_op()
{
  buffers::vote_operation vote_op;
  vote_op.set_voter( "initminer" );
  vote_op.set_author( "initminer" );
  vote_op.set_permlink( "/" );
  vote_op.set_weight( 100 );

  buffers::operation op;
  *op.mutable_vote_op() = vote_op;

  return op;
}

operation get_hive_op()
{
  vote_operation op;
  op.voter    = "initminer";
  op.author   = "initminer";
  op.permlink = "/";
  op.weight   = 100;

  return op;
}

buffers::transaction get_protobuf_tx()
{
  buffers::transaction tx;
  tx.set_ref_block_num( 1 );
  tx.set_ref_block_prefix( 2 );

  tx.set_expiration( 10 );
  *tx.add_operations() = get_protobuf_op();
  tx.clear_extensions();

  return tx;
}

transaction get_hive_tx()
{
  transaction tx;
  tx.ref_block_num    = 1;
  tx.ref_block_prefix = 2;

  tx.expiration = fc::time_point_sec{ 10 };
  tx.operations = { get_hive_op() };
  tx.extensions = {};

  return tx;
}

std::string get_hex( const uint8_t* data, size_t len )
{
  std::string result;
  result.resize( len * 2 );
  static const char* to_hex = "0123456789abcdef";
  uint8_t* c                = (uint8_t*) data;
  for( uint32_t i = 0; i < len; ++i )
  {
    result[i * 2]     = to_hex[( c[i] >> 4 )];
    result[i * 2 + 1] = to_hex[( c[i] & 0x0f )];
  }
  return result;
}

template< typename T >
std::string pack_protobuf( const T& protobuf_obj )
{
  std::string protobuf_data;
  protobuf_obj.SerializeToString( &protobuf_data );

  return protobuf_data;
}

template< typename T >
std::vector< char > pack_hive( const T& hive_obj )
{
  return fc::raw::pack_to_vector( hive_obj );
}

template< typename T >
std::string protobuf_to_hex( const T& protobuf_obj )
{
  std::string protobuf_data = pack_protobuf( protobuf_obj );

  return get_hex( reinterpret_cast< const uint8_t* >( protobuf_data.data() ), protobuf_data.size() );
}

template< typename T >
std::string hive_to_hex( const T& hive_obj )
{
  std::vector< char > fc_data = pack_hive( hive_obj );

  return get_hex( reinterpret_cast< const uint8_t* >( fc_data.data() ), fc_data.size() );
}

} // namespace

TEST( vote_operation, test_op_serialization )
{
  // Compare hex representations of different serializations
  EXPECT_EQ( hive_to_hex( get_hive_op() ), // serialize hive operation (static_variant holding vote_operation)
    "0009696e69746d696e657209696e69746d696e6572012f6400" );
  EXPECT_EQ( protobuf_to_hex( get_protobuf_op() ), // serialize protobuf operation (oneof holding vote_operation)
    "0a1b0a09696e69746d696e65721209696e69746d696e65721a012f2064" );
}

TEST( vote_operation, test_tx_serialization )
{
  // Compare hex representations of different serializations
  EXPECT_EQ( hive_to_hex( get_hive_tx() ), // serialize hive transaction (with different values - see ::get_hive_tx implementation)
    "0100020000000a000000010009696e69746d696e657209696e69746d696e6572012f640000" );
  EXPECT_EQ( protobuf_to_hex( get_protobuf_tx() ), // serialize protobuf transaction (with different values - see ::get_protobuf_tx implementation)
    "08011002180a221d0a1b0a09696e69746d696e65721209696e69746d696e65721a012f2064" );
}
